// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.7.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract TokenBNBERC20 is ERC20('Mixsome', 'SOME') {
    constructor () {
        _mint(msg.sender, 33722491909385117600000000);
    }
}
