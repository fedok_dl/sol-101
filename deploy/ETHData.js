/* eslint-disable */

const Helper = require('../test/helpers/helper');

const h = new Helper();

class ETHData {
  getData() {
    const data = {
      privateRound1: {
        balances: [
          h.toWei('1792114.6953405').toString(),
          h.toWei('4039426.5232975').toString(),
          h.toWei('3584229.390681').toString(),
          h.toWei('1792114.6953405').toString(),
          h.toWei('1792114.6953405').toString(),
        ],
        balancesBase: [],
        percents: h.arrToBNDec([0.15, 0.08, 0.08, 0.08, 0.08, 0.08, 0.08, 0.07, 0.06, 0.06, 0.06, 0.06, 0.06]),
        addresses: [
          '0xFcbf806792f06d9c78E50B3737E1a22cfC36a942',
          '0xA8437E2e7737E38bF3817D14EC2e03f4C28a01A7',
          '0xc90399D11E238EBAB1aA640a83aFF4db0BB374eb',
          '0xee90B12BAa7CB7e286C646Ff9Bc09Aa333401A81',
          '0xdF936D8F3550009CBa6ceaE6803cf79C1AAc573d',
        ],
      },
      privateRound2: {
        balances: [
          h.toWei('1294498.38187702000000').toString(),
          h.toWei('647249.19093851100000').toString(),
          h.toWei('1941747.57281553000000').toString(),
          h.toWei('1294498.38187702000000').toString(),
          h.toWei('258899.67637540500000').toString(),
          h.toWei('2588996.76375405000000').toString(),
          h.toWei('2588996.76375405000000').toString(),
          h.toWei('4676375.40453074000000').toString(),
          h.toWei('129449.83818770200000').toString(),
          h.toWei('90614.88673139160000').toString(),
          h.toWei('90614.88673139160000').toString(),
          h.toWei('90614.88673139160000').toString(),
          h.toWei('64724.91909385110000').toString(),
          h.toWei('129449.83818770200000').toString(),
          h.toWei('64724.91909385110000').toString(),
          h.toWei('64724.91909385110000').toString(),
          h.toWei('90614.88673139160000').toString(),
          h.toWei('90614.88673139160000').toString(),
          h.toWei('64724.91909385110000').toString(),
          h.toWei('90614.88673139160000').toString(),
          h.toWei('271844.66019417500000').toString(),
          h.toWei('90614.88673139160000').toString(),
          h.toWei('90614.88673139160000').toString(),
          h.toWei('90614.88673139160000').toString(),
          h.toWei('64724.91909385110000').toString(),
          h.toWei('129449.83818770200000').toString(),
          h.toWei('77669.90291262140000').toString(),
          h.toWei('32362.45954692560000').toString(),
          h.toWei('90614.88673139160000').toString(),
          h.toWei('77669.90291262140000').toString(),
          h.toWei('51779.93527508090000').toString(),
          h.toWei('77669.90291262140000').toString(),
          h.toWei('90614.88673139160000').toString(),
          h.toWei('84142.39482200650000').toString(),
          h.toWei('84142.39482200650000').toString(),
          h.toWei('77669.90291262140000').toString(),
          h.toWei('90614.88673139160000').toString(),
          h.toWei('90614.88673139160000').toString(),
          h.toWei('122977.34627831700000').toString(),
          h.toWei('388349.51456310700000').toString(),
        ],
        balancesBase: [],
        percents: h.arrToBNDec([0.15, 0.08, 0.08, 0.08, 0.08, 0.08, 0.08, 0.07, 0.06, 0.06, 0.06, 0.06, 0.06]),
        addresses: [
          '0xFcbf806792f06d9c78E50B3737E1a22cfC36a942',
          '0xee90B12BAa7CB7e286C646Ff9Bc09Aa333401A81',
          '0xdF936D8F3550009CBa6ceaE6803cf79C1AAc573d',
          '0xC02ad7b9a9121fc849196E844DC869D2250DF3A6',
          '0x457d0cB2bA7ecAb58612FD2cF9d30885CA06FFA5',
          '0xE78E525f14590b4F4923521E72a7a3Ae183f4754',
          '0x082878a39693BEEa5684A68789b7211cA7ae7221',
          '0xc90399D11E238EBAB1aA640a83aFF4db0BB374eb',
          '0x8888888888E9997E64793849389a8Faf5E8e547C',
          '0x3943b578D00D61b622Fd99AB6F16921e54F7612b',
          '0xe09A29e3caC66cAd03A31AdE68a440A08c7D5BD8',
          '0x2165Ffc3b1c7eE4A618ecDf3D025625f30112E4E',
          '0x9b7686c3af3f6Fb73374E1dC89D971335f09fAFb',
          '0xE7521950426479d525381940604D27aeC12FE97a',
          '0x2e61fB07c4DDfFC9ba291E36E9dE4D8e1Cc4DF7A',
          '0x25ed2f006BEB73c810135AA4f51FFB39CFd4bb76',
          '0xc44661e7271B38c403cFc7F6C87200869798f39C',
          '0x93511aCC70f19D125Ca158910CB983D1a8C2F74d',
          '0x29Bf6652e795C360f7605be0FcD8b8e4F29a52d4',
          '0x38f2944e482a050942e5fb1652af4690017cd141',
          '0xA34673CeC67e5CfbDE1Bc5E7857f1B67e12d1939',
          '0x545E333e083776F8ae69bD41B6268AeA07723C02',
          '0x2D69BAB9738b05048be16DE3E5E0A945b8EeEf3a',
          '0x3c8972bA9D1Ba82acf390ab6e7fCC29dD3a7c53E',
          '0xa1d0B3B360595A82dbAAC1667535579bC568F8DD',
          '0x0Ed67dAaacf97acF041cc65f04A632a8811347fF',
          '0x4553eD5d8d3731E629f67BD86abd021175F31848',
          '0xD2Ef10da66727627C68bEd148e881C923C1baA77',
          '0x82Ba7508f7F1995AB1623258D66Cb4E2B2b8F467',
          '0x1ad5f28a4775f8292eedd3288ff760dd9e825d05',
          '0x11EfF19DC599ee676b6D65bae0A60479a87e889A',
          '0x749E7333FC2b1997E546835b6F8353c628121E5a',
          '0x48d6FC0ccDa84241Fb57eb2d785A0BdcA03e58c6',
          '0x2BA162F2D358386Bbe207D817E87DB2faef6C948',
          '0xF76dbc5d9A7465EcEc49700054bF27f88cf9ad05',
          '0x60a70bcBec9447060916f5B3F63f62c526e83360',
          '0x4d47Fa947202D44dF94C4C38C85663CEf937352e',
          '0x788B0A2D87a16A3F745B0609D1b931773D10ccFF',
          '0x7a164f6DAde07Bcac283391D75E12c0Aeb20B65D',
          '0x27f7E530d0c1aB780d8d6962e9Cfd45a9C96d8b9',
        ],
      },
      marketing: {
        balances: [
          h.toWei('6033549.78354978000000').toString(),
          h.toWei('216450.21645022').toString(),
        ],
        balancesBase: [],
        percents: h.arrToBNDec([0.15, 0.085, 0.085, 0.085, 0.085, 0.085, 0.075, 0.075, 0.075, 0.05, 0.05, 0.05, 0.05]),
        addresses: [
          '0xdfD99d50adfA35D896Eaa48AfB0CFFDD4eb7b80c',
          '0x9ddea6c0bfa53bdf244ade7ba5ed7e227f6e58b4',
        ],
      },
      liquidity: {
        balances: [
          h.toWei('20000000').toString(),
        ],
        balancesBase: [],
        percents: h.arrToBNDec([0.07, 0.085, 0.085, 0.085, 0.075, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05,
          0.05, 0.05, 0.05, 0.05]),
        addresses: [
          '0xAC8cEEa416a943976C6D08c78a1ae77309F86de1',
        ],
      },
      team: {
        balances: [
          h.toWei('15000000').toString(),
        ],
        balancesBase: [],
        percents: h.arrToBNDec([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.1, 0.075, 0.075, 0.1, 0.075, 0.075,
          0.1, 0.075, 0.075, 0.1, 0.075, 0.075]),
        addresses: [
          '0x5bFC1A48Ff385A71f20bAa2b77Ff0a30d6C3257b',
        ],
      },
      advisors: {
        balances: [
          h.toWei('5000000').toString(),
        ],
        balancesBase: [],
        percents: h.arrToBNDec([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.1, 0.075, 0.075, 0.1, 0.075, 0.075,
          0.1, 0.075, 0.075, 0.1, 0.075, 0.075]),
        addresses: [
          '0x5bFC1A48Ff385A71f20bAa2b77Ff0a30d6C3257b',
        ],
      },
      development: {
        balances: [
          h.toWei('16000000').toString(),
        ],
        balancesBase: [],
        percents: h.arrToBNDec([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.1, 0.075, 0.075, 0.1, 0.075, 0.075,
          0.1, 0.075, 0.075, 0.1, 0.075, 0.075]),
        addresses: [
          '0x5bFC1A48Ff385A71f20bAa2b77Ff0a30d6C3257b',
        ],
      },
    };

    this.validateData(data);

    return data;
  }

  validateData(data) {
    let totalSum = h.toBN('0');

    for (const groupName in data) {
      let sum = h.toBN('0');
      for (let i = 0; i < data[groupName].balances.length; i++) {
        sum = sum.plus(data[groupName].balances[i]);
      }

      totalSum = totalSum.plus(sum);

      sum = h.fromWei(sum).toString();

      switch (groupName) {
        case 'privateRound1':
          if (sum !== '13000000') throw { message: 'Invalid privateRound1' };
          break;
        case 'privateRound2':
          if (sum !== '18527508.0906148824') throw { message: 'Invalid privateRound2' };
          break;
        case 'marketing':
          if (sum !== '6250000') throw { message: 'Invalid marketing' };
          break;
        case 'liquidity':
          if (sum !== '20000000') throw { message: 'Invalid liquidity' };
          break;
        case 'team':
          if (sum !== '15000000') throw { message: 'Invalid team' };
          break;
        case 'advisors':
          if (sum !== '5000000') throw { message: 'Invalid advisors' };
          break;
        case 'development':
          if (sum !== '16000000') throw { message: 'Invalid development' };
          break;
        default:
          throw { message: 'Group not found' };
      }
    }

    // console.log(totalSum.toString());

    totalSum = totalSum
      .plus(h.toWei('6472491.9093851176'))
      .plus(h.toWei('1000000'))
      .plus(h.toWei('6250000'))
      .plus(h.toWei('20000000'));

    if (h.fromWei(totalSum).toString() !== '127500000') {
      throw { message: 'Invalid total sum' };
    }
  }
}

module.exports = ETHData;

