/* eslint-disable */

const Helper = require('../test/helpers/helper');

const h = new Helper();

class ETHData {
  getData() {
    const data = {
      unlockedTokenFromPR2: {
        balances: [
          h.toWei('6472491.9093851176').toString(),
        ],
        balancesBase: [],
        percents: h.arrToBNDec([1]),
        addresses: [
          '0xa594af6496a61423582B19319732e3C7DC072f9F',
        ],
      },
      publicSale: {
        balances: [
          h.toWei('1000000').toString(),
        ],
        balancesBase: [],
        percents: h.arrToBNDec([1]),
        addresses: [
          '0xbF0A42b9276f4c851D835262D493C6E19fcB324f',
        ],
      },
      marketing: {
        balances: [
          h.toWei('6250000').toString(),
        ],
        balancesBase: [],
        percents: h.arrToBNDec([0.15, 0.085, 0.085, 0.085, 0.085, 0.085, 0.075, 0.075, 0.075, 0.05, 0.05, 0.05, 0.05]),
        addresses: [
          '0xdfD99d50adfA35D896Eaa48AfB0CFFDD4eb7b80c',
        ],
      },
      liquidity: {
        balances: [
          h.toWei('20000000').toString(),
        ],
        balancesBase: [],
        percents: h.arrToBNDec([0.07, 0.085, 0.085, 0.085, 0.075, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05, 0.05,
          0.05, 0.05, 0.05, 0.05]),
        addresses: [
          '0xAC8cEEa416a943976C6D08c78a1ae77309F86de1',
        ],
      },
    };

    this.validateData(data);

    return data;
  }

  validateData(data) {
    let totalSum = h.toBN('0');

    for (const groupName in data) {
      let sum = h.toBN('0');
      for (let i = 0; i < data[groupName].balances.length; i++) {
        sum = sum.plus(data[groupName].balances[i]);
      }

      totalSum = totalSum.plus(sum);

      sum = h.fromWei(sum).toString();

      switch (groupName) {
        case 'unlockedTokenFromPR2':
          if (sum !== '6472491.9093851176') throw { message: 'Invalid privateRound2' };
          break;
        case 'publicSale':
          if (sum !== '1000000') throw { message: 'Invalid publicSale' };
          break;
        case 'marketing':
          if (sum !== '6250000') throw { message: 'Invalid marketing' };
          break;
        case 'liquidity':
          if (sum !== '20000000') throw { message: 'Invalid liquidity' };
          break;
        default:
          throw { message: 'Group not found' };
      }
    }

    // console.log(totalSum.toString());

    totalSum = totalSum.plus(h.toWei('93777508.0906148824'));

    if (h.fromWei(totalSum).toString() !== '127500000') {
      throw { message: 'Invalid total sum' };
    }
  }
}

module.exports = ETHData;
