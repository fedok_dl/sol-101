const Root = artifacts.require('Root');
const Token = artifacts.require('TokenBNBERC20');

const BNBData = require('./BNBData');

const Helper = require('../test/helpers/helper');

module.exports = async (callback) => {
  console.log('Begin deploy!\n');

  try {
    const h = new Helper();

    const data = (new BNBData()).getData();

    const tokenContract = await Token.new();
    console.log('BNB ERC20 token contract address:', tokenContract.address);

    const rootContract = await Root.new(tokenContract.address);
    console.log('BNB root contract address:', rootContract.address);

    await tokenContract.transfer(rootContract.address, h.toBN('33722491909385117600000000'));

    // await rootContract.loadGroupInfo(data.privateRound1, 0);
    // await rootContract.loadGroupInfo(data.privateRound2, 1);
    await rootContract.loadGroupInfo(data.unlockedTokenFromPR2, 2);
    await rootContract.loadGroupInfo(data.publicSale, 3);
    await rootContract.loadGroupInfo(data.marketing, 4);
    await rootContract.loadGroupInfo(data.liquidity, 5);
    // await rootContract.loadGroupInfo(data.team, 6);
    // await rootContract.loadGroupInfo(data.advisors, 7);
    // await rootContract.loadGroupInfo(data.development, 8);

    console.log('\nDone!');
  } catch (e) {
    console.log(e.message);
  }

  callback();
};
