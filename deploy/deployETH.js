const Root = artifacts.require('Root');
const Token = artifacts.require('TokenERC20');

const ETHData = require('./ETHData');

const Helper = require('../test/helpers/helper');

module.exports = async (callback) => {
  console.log('Begin deploy!\n');

  try {
    const h = new Helper();

    const data = (new ETHData()).getData();

    const tokenContract = await Token.new();
    console.log('ERC20 token contract address:', tokenContract.address);

    const rootContract = await Root.new(tokenContract.address);
    console.log('Root contract address:', rootContract.address);

    await tokenContract.transfer(rootContract.address, h.toBN('93777508090614882400000000'));

    await rootContract.loadGroupInfo(data.privateRound1, 0);
    await rootContract.loadGroupInfo(data.privateRound2, 1);
    // await rootContract.loadGroupInfo(data.unlockedTokenFromPR2, 2);
    // await rootContract.loadGroupInfo(data.publicSale, 3);
    await rootContract.loadGroupInfo(data.marketing, 4);
    await rootContract.loadGroupInfo(data.liquidity, 5);
    await rootContract.loadGroupInfo(data.team, 6);
    await rootContract.loadGroupInfo(data.advisors, 7);
    await rootContract.loadGroupInfo(data.development, 8);

    console.log('\nDone!');
  } catch (e) {
    console.log(e.message);
  }

  callback();
};
